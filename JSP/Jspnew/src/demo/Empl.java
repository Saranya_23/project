package demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Empl
 */
public class Empl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Empl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String id=request.getParameter("id");
		String Name= request.getParameter("na");
		String Age= request.getParameter("age");
		String Salary= request.getParameter("sal");
		String Design= request.getParameter("des");
         try {
		Class.forName("oracle.jdbc.driver.OracleDriver");
     	Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe","system","tiger");
		PreparedStatement ps=con.prepareStatement("Insert into employee values(?,?,?,?,?)");
		
		ps.setString(1, id);
		ps.setString(2, Name);
		ps.setString(3, Age);
		ps.setString(4, Salary);
		ps.setString(5, Design);
        ps.execute();
        out.println("Record Inserted ");
        out.print("<body style=\"background-image:url('starsky.jpg'); background-size: cover;\">success</body>");
// 
//	if(response.getElementById("age").value>60)
//	{
//	alert("age must be less than 60")
//	return false;
//	}
//	if(response.getElementById("sal").value<25000)
//	{
//	alert("Salary must be minimum 25000")
//	return false;
//	}
         }
         catch (Exception e)
         {
        	 out.print(e);
        	 out.print(" <a href='NewLogin.html> Thank You </a>");
         }
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
